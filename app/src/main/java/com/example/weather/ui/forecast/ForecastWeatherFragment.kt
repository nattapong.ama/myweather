package com.example.weather.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.weather.R
import com.example.weather.viewmodel.ForecastWeatherViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weather.vo.forecast.ForecastWeatherResponse
import com.example.weather.vo.forecast.HourlyItem
import kotlinx.android.synthetic.main.fragment_forecast_weather.*

class ForecastWeatherFragment : Fragment() {

    private val viewModel: ForecastWeatherViewModel by viewModel()

    private var cityName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forecast_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let { arguments ->
            val args = ForecastWeatherFragmentArgs.fromBundle(arguments)
            cityName = args.cityName
        }

        loadData()
    }

    private fun loadData() {
        viewModel.result().observe(viewLifecycleOwner,
            Observer<ForecastWeatherResponse> {
                if (it != null) {
                    updateAdapter(it.hourly!!)
                }
            })

        viewModel.forecastWeather(cityName)
    }

    private fun updateAdapter(items: List<HourlyItem>) {
        rvForecastWeather.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            this.adapter = ForecastWeatherAdapter(items)
        }
    }

    override fun onDestroy() {
        viewModel.disposeElements()
        super.onDestroy()
    }

}