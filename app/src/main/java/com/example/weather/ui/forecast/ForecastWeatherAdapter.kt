package com.example.weather.ui.forecast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.R
import com.example.weather.utils.toDateString
import com.example.weather.vo.forecast.HourlyItem
import kotlinx.android.synthetic.main.item_forecast_weather.view.*

class ForecastWeatherAdapter(private val items: List<HourlyItem>?) : RecyclerView.Adapter<ForecastWeatherAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_forecast_weather, parent, false))
    }

    override fun getItemCount(): Int {
        return items?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items!![position])
    }

    class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {

        fun bind(item: HourlyItem) {
            itemView.tvDateTime.text = item.dt.toLong().toDateString()
            itemView.tvTemp.let {
                it.text = it.context.getString(R.string.forecast_temp_format, item.temp.toInt().toString())
            }
        }
    }

}