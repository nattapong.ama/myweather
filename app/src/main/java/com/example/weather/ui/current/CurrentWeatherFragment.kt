package com.example.weather.ui.current

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.weather.R
import com.example.weather.utils.Status
import com.example.weather.viewmodel.CurrentWeatherViewModel
import kotlinx.android.synthetic.main.fragment_current_weather.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.example.weather.utils.toDateString
import kotlinx.android.synthetic.main.fragment_current_weather.tvCityName

class CurrentWeatherFragment : Fragment() {

    private val model: CurrentWeatherViewModel by viewModel()

    private var cityName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_current_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { arguments ->
            val args = CurrentWeatherFragmentArgs.fromBundle(arguments)
            cityName = args.cityName
        }

        loadData()
        setupListeners()
    }

    private fun loadData() {
        model.currentWeather(cityName).observe(activity as FragmentActivity, Observer {
            when (it?.status) {
                Status.SUCCESS -> {
                    tvCityName.text = it.data?.name
                    tvTemp.text = it.data?.main?.temp?.toInt().toString()
                    tvMaxTemp.text = "${it.data?.main?.temp_max?.toInt().toString()}°C "
                    tvMinTemp.text = "${it.data?.main?.temp_min?.toInt().toString()}°C "
                    tvHumidity.text = "Humidity: ${it.data?.main?.humidity.toString()}"
                    tvWeatherDescription.text = it.data?.weather!![0].description
                }
                else -> {

                }
            }
        })
    }

    private fun setupListeners() {
        val action = CurrentWeatherFragmentDirections.actionCurrentWeatherFragment2ToForecastWeatherFragment2(
            cityName = cityName!!
        )

        currentWeatherButton.setOnClickListener { buttonView ->
            buttonView
                .findNavController()
                .navigate(action)
        }
    }

}