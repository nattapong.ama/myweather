package com.example.weather.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.weather.api.ApiService
import com.example.weather.utils.AppExecutors
import com.example.weather.utils.NoNetworkException
import com.example.weather.utils.Resource
import com.example.weather.vo.current.CurrentWeatherResponse
import retrofit2.Call

class CurrentWeatherRepo(private val appExecutors: AppExecutors, private val apiService: ApiService) {

    fun getCurrentWeather(cityName: String?): LiveData<Resource<CurrentWeatherResponse>> {
        val data = MutableLiveData<Resource<CurrentWeatherResponse>>()
        data.postValue(Resource.loading(null))
        NetworkBoundResource(appExecutors, object : NetworkBoundResource.Callback<CurrentWeatherResponse, CurrentWeatherResponse>() {

            override fun onUnauthorized() {
                data.postValue(Resource.unauthorized("", null))
            }

            override fun onCreateCall(): Call<CurrentWeatherResponse> {
                return apiService.currentWeather(cityName, "metric", "417c593a441f23c4a6d9b5093fd63410")
            }

            override fun onResponse(code: Int, message: String, remoteData: CurrentWeatherResponse?) {
                when (code) {
                    in 200..201 -> {
                        if (remoteData != null) {
                            appExecutors.diskIO().execute {
                                data.postValue(Resource.success(remoteData))
                            }
                        } else {
                            data.postValue(Resource.error(message, null))
                        }
                    }
                    else -> {
                        data.postValue(Resource.error(message, null))
                    }
                }
            }

            override fun onError(t: Throwable) {
                data.postValue(Resource.error(t.message!!, null))
            }

            override fun onWarning(t: Throwable) {
                data.postValue(Resource.warning(t.message!!, null))
            }

            override fun onNetworkUnavailable(t: NoNetworkException) {
                data.postValue(Resource.network(t.message!!, null))
            }

            override fun enableLoadFromDb(): Boolean = NetworkBoundResource.LOAD_FROM_DB_DISABLE

        })

        return data
    }

}