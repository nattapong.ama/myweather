package com.example.weather.repo

import android.util.Log
import com.example.weather.api.ApiService
import com.example.weather.utils.AppExecutors
import com.example.weather.vo.forecast.ForecastWeatherResponse
import com.example.weather.vo.location.AddressResponse
import io.reactivex.Observable

class ForecastWeatherRepo(private val appExecutors: AppExecutors, private val apiService: ApiService) {

    fun getForecastWeather(lat: String?, lng: String?, apiKey: String?): Observable<ForecastWeatherResponse> {
        return apiService.forecastWeather(lat, lng, "metric", apiKey)
            .doOnNext {
                Log.d("REPOSITORY *** ", it.toString())
            }
    }

    fun getLocation(cityName: String?, googleApiKey: String?): Observable<AddressResponse> {
        return apiService.getLocation("https://maps.googleapis.com/maps/api/geocode/json?address=$cityName&key=$googleApiKey")
            .doOnNext {
                Log.d("REPOSITORY *** ", it.toString())
            }
    }

}