package com.example.weather.vo.forecast

data class AlertsItem(val start: Int = 0,
                      val description: String = "",
                      val senderName: String = "",
                      val end: Int = 0,
                      val event: String = "")