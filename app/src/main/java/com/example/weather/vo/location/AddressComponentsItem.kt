package com.example.weather.vo.location

data class AddressComponentsItem(val types: List<String>?,
                                 val shortName: String = "",
                                 val longName: String = "")