package com.example.weather.vo.forecast

data class MinutelyItem(val dt: Int = 0,
                        val precipitation: String = "")