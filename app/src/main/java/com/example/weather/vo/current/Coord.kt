package com.example.weather.vo.current

data class Coord(
    val lat: Double,
    val lon: Double
)