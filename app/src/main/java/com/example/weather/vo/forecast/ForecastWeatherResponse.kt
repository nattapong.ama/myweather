package com.example.weather.vo.forecast

data class ForecastWeatherResponse(val alerts: List<AlertsItem>?,
                                   val current: Current,
                                   val timezone: String = "",
                                   val timezoneOffset: String = "",
                                   val daily: List<DailyItem>?,
                                   val lon: String = "",
                                   val hourly: List<HourlyItem>?,
                                   val minutely: List<MinutelyItem>?,
                                   val lat: Double = 0.0)