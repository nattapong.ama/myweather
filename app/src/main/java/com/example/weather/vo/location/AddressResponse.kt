package com.example.weather.vo.location

data class AddressResponse(val results: List<ResultsItem>?,
                           val status: String = "")