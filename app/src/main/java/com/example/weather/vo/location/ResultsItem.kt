package com.example.weather.vo.location

data class ResultsItem(val formattedAddress: String = "",
                       val types: List<String>?,
                       val geometry: Geometry,
                       val addressComponents: List<AddressComponentsItem>?,
                       val plusCode: PlusCode,
                       val placeId: String = "")