package com.example.weather.vo.location

data class Geometry(val viewport: Viewport,
                    val location: Location,
                    val locationType: String = "")