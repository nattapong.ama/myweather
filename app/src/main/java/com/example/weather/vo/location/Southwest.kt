package com.example.weather.vo.location

data class Southwest(val lng: Double = 0.0,
                     val lat: Double = 0.0)