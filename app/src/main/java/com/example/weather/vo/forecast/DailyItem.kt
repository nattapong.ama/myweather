package com.example.weather.vo.forecast

data class DailyItem(val rain: String = "",
                     val sunrise: String = "",
                     val temp: Temp,
                     val uvi: String = "",
                     val pressure: String = "",
                     val clouds: String = "",
                     val feelsLike: FeelsLike,
                     val dt: String = "",
                     val pop: String = "",
                     val windDeg: String = "",
                     val dewPoint: String = "",
                     val sunset: String = "",
                     val weather: List<WeatherItem>?,
                     val humidity: String = "",
                     val windSpeed: String = "")