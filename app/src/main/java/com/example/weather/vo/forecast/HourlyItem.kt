package com.example.weather.vo.forecast

data class HourlyItem(val rain: Rain,
                      val temp: Double = 0.0,
                      val visibility: String = "",
                      val pressure: String = "",
                      val clouds: String = "",
                      val feelsLike: String = "",
                      val dt: String = "",
                      val pop: String = "",
                      val windDeg: String = "",
                      val dewPoint: String = "",
                      val weather: List<WeatherItem>?,
                      val humidity: String = "",
                      val windSpeed: String = "")