package com.example.weather.vo.current

data class Wind(
    val deg: Int,
    val speed: Double
)