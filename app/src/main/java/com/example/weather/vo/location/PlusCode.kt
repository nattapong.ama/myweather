package com.example.weather.vo.location

data class PlusCode(val compoundCode: String = "",
                    val globalCode: String = "")