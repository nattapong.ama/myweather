package com.example.weather.vo.forecast

data class FeelsLike(val eve: Double = 0.0,
                     val night: Double = 0.0,
                     val day: Double = 0.0,
                     val morn: Double = 0.0)