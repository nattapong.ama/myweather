package com.example.weather.vo.location

data class Location(val lng: Double = 0.0,
                    val lat: Double = 0.0)