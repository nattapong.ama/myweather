package com.example.weather.vo.forecast

data class WeatherItem(val icon: String = "",
                       val description: String = "",
                       val main: String = "",
                       val id: String = "")