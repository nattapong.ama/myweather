package com.example.weather.vo.location

data class Viewport(val southwest: Southwest,
                    val northeast: Northeast)