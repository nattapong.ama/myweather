package com.example.weather.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.weather.repo.CurrentWeatherRepo
import com.example.weather.utils.Resource
import com.example.weather.vo.current.CurrentWeatherResponse

class CurrentWeatherViewModel(private val repo: CurrentWeatherRepo): ViewModel() {

    fun currentWeather(cityName: String?): LiveData<Resource<CurrentWeatherResponse>> {
        return repo.getCurrentWeather(cityName)
    }

}