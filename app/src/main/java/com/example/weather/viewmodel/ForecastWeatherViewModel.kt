package com.example.weather.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weather.BuildConfig
import com.example.weather.repo.ForecastWeatherRepo
import com.example.weather.vo.forecast.ForecastWeatherResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ForecastWeatherViewModel(private val repo: ForecastWeatherRepo): ViewModel() {

    var cryptocurrenciesResult: MutableLiveData<ForecastWeatherResponse> = MutableLiveData()
    var cryptocurrenciesError: MutableLiveData<String> = MutableLiveData()
    var cryptocurrenciesLoader: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var disposableObserver: DisposableObserver<ForecastWeatherResponse>

    fun result(): LiveData<ForecastWeatherResponse> {
        return cryptocurrenciesResult
    }

    fun error(): LiveData<String> {
        return cryptocurrenciesError
    }

    fun loader(): LiveData<Boolean> {
        return cryptocurrenciesLoader
    }

    fun forecastWeather(cityName: String?) {
        disposableObserver = object : DisposableObserver<ForecastWeatherResponse>() {
            override fun onComplete() {

            }

            override fun onNext(cryptocurrencies: ForecastWeatherResponse) {
                cryptocurrenciesResult.postValue(cryptocurrencies)
                cryptocurrenciesLoader.postValue(false)
            }

            override fun onError(e: Throwable) {
                cryptocurrenciesError.postValue(e.message)
                cryptocurrenciesLoader.postValue(false)
            }
        }

        repo.getLocation(cityName, BuildConfig.GOOGLE_API_KEY)
            .flatMap {
                val location = it.results?.get(0)?.geometry?.location
                repo.getForecastWeather(location?.lat.toString(), location?.lng.toString(), BuildConfig.WEATHER_API_KEY)
            }
            .filter {
                it.lat == 2.1
            }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(disposableObserver)
    }

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
    }

}