package com.example.weather.di

import com.example.weather.api.ApiService
import com.example.weather.api.ServiceGenerator
import com.example.weather.repo.CurrentWeatherRepo
import com.example.weather.repo.ForecastWeatherRepo
import com.example.weather.utils.AppExecutors
import com.example.weather.utils.LiveNetworkMonitor
import com.example.weather.utils.MainThreadExecutor
import com.example.weather.viewmodel.CurrentWeatherViewModel
import com.example.weather.viewmodel.ForecastWeatherViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.util.concurrent.Executors

val appModule = module {

    single { LiveNetworkMonitor(androidContext()) }

    single { AppExecutors(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3), MainThreadExecutor()) }

    factory {
        ServiceGenerator.create(
            androidContext(),
            get(),
            ApiService::class.java
        )
    }

    single { CurrentWeatherRepo(get(), get()) }

    viewModel { CurrentWeatherViewModel(get()) }

    single { ForecastWeatherRepo(get(), get()) }

    viewModel { ForecastWeatherViewModel(get()) }

}