package com.example.weather.api

import android.content.Context
import com.example.weather.utils.LiveNetworkMonitor
import com.example.weather.utils.NoNetworkException
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


object ServiceGenerator {

    private var baseUrl = "https://api.openweathermap.org/data/2.5/"

    fun <T> create(context: Context, networkMonitor: LiveNetworkMonitor, clazz: Class<T>): T {

        val cacheDir = File(context.cacheDir, UUID.randomUUID().toString())
        // 10 MiB cache
        val cache = Cache(cacheDir, 10 * 1024 * 1024)

        val httpClient = OkHttpClient.Builder()
                .cache(cache)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)

        // Network monitor interceptor:
        httpClient.addInterceptor { chain ->
            if (networkMonitor.isConnected()) {
                return@addInterceptor chain.proceed(chain.request())
            } else {
                throw NoNetworkException()
            }
        }

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(interceptor)

        val client = httpClient.build()
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(client)
                .build()

        return retrofit.create(clazz)
    }

}