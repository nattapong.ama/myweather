package com.example.weather.api

import com.example.weather.vo.current.CurrentWeatherResponse
import com.example.weather.vo.forecast.ForecastWeatherResponse
import com.example.weather.vo.location.AddressResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService {

    @GET("weather")
    fun currentWeather(
        @Query("q") cityName: String?,
        @Query("units") units: String?,
        @Query("appid") apiKey: String?): Call<CurrentWeatherResponse>

    @GET("onecall")
    fun forecastWeather(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?,
        @Query("units") units: String?,
        @Query("appid") apiKey: String?): Observable<ForecastWeatherResponse>

    @GET
    fun getLocation(@Url url: String?): Observable<AddressResponse>

}