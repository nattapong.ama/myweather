package com.example.weather.utils

interface NetworkMonitor {

    fun isConnected(): Boolean

}