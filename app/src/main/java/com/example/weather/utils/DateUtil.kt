package com.example.weather.utils

import android.text.format.DateFormat
import java.util.*

fun Long.toDateString() : String {
    val cal: Calendar = Calendar.getInstance(Locale.ENGLISH)
    cal.timeInMillis = this * 1000L
    return DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString()
}